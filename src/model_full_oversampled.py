from datetime import datetime
from tempfile import mkdtemp
from shutil import rmtree
from joblib import Memory

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import SequentialFeatureSelector
from sklearn.ensemble import VotingClassifier

from modelling import modelling, evaluation
from preprocessing import feature_engineering, preprocess
from utils import save_outputs

start_time = datetime.now()

base_dir = ".."
df = pd.read_csv(base_dir + '/data/train.csv')
save_output = True

# Create a temporary folder to store the transformers of the pipeline
cachedir = mkdtemp()
memory = Memory(location=cachedir, verbose=3)

# Load data. Drop columns according to analysis
cat_columns_to_drop = ['c1', 'c2', 'c9', 'c10', 'c11', 'c12']
num_cols_to_drop = ['x2', 'x4', 'x5', 'x6', 'x8', 'x10', 'x12', 'x14', 'x16', 'x20', 'x22', 'x23', 'x24', 'x32', 'x35', 'o2']
columns_to_drop = cat_columns_to_drop + num_cols_to_drop
df.drop(columns=columns_to_drop, inplace=True)
X = df.drop(columns=['i', 'y'])
y = df['y']

X, y = preprocess.oversampler(X, y)

num_col_names = [name for name in df.columns.tolist() if name.startswith('x')]
cat_col_names = [name for name in df.columns.tolist() if name.startswith('c')]
ord_col_names = [name for name in df.columns.tolist() if name.startswith('o')]

n_features = len(num_col_names) + len(cat_col_names) + len(ord_col_names)

clf_selection = modelling.Classifiers_sk.svm.value
sfs = SequentialFeatureSelector(clf_selection, direction='backward', n_jobs=-1, scoring='accuracy')

engineered_data = feature_engineering.FeatureEngineering(
    num_col_names, cat_col_names, ord_col_names, True
)

pipe_selection = Pipeline([
    ('preprocessing', engineered_data.preprocessor),
    ('feature_selection', sfs),
    ('classifier', clf_selection) # if a classifier is not added, grid search yields a warning, not working as expected, with (runs but fails): "UserWarning: Scoring failed. The score on this train-test partition for these parameters will be set to nan. Details: (...) AttributeError: 'SequentialFeatureSelector' object has no attribute 'predict'"
], memory=memory)

search_space_selection = [
    {'feature_selection__n_features_to_select': [0.90, 0.65, 0.35]}
]

grid_selector = GridSearchCV(pipe_selection, search_space_selection, cv=3, n_jobs=-1, verbose=3, scoring='accuracy')
grid_selector.fit(X, y)

time_feature_selection_end = datetime.now()
run_time_feature_selection = time_feature_selection_end - start_time

feature_selector = grid_selector.best_estimator_
feature_selector.steps.pop(len(feature_selector.steps)-1) # drop classifier step from pipeline. only want the feaeture selection part of the pipeline
selected_features_indices = feature_selector.named_steps['feature_selection'].support_
selected_features_n = feature_selector.named_steps['feature_selection'].n_features_to_select_
features_names = np.concatenate((X.columns.drop(['c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'o1']).to_numpy(), feature_selector.named_steps['preprocessing'].named_transformers_['cat'].named_steps['onehot'].get_feature_names(input_features=['c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'o1'])))
selected_features_ids = [v for (k, v) in list(zip(selected_features_indices, features_names)) if k]

selection_results = pd.DataFrame(grid_selector.cv_results_)
selection_results = selection_results.sort_values("mean_test_score", ascending=False)

msg = (
    "****************** BACKWARD ELIMINATION (AUTO) ******************\n"
    "Features: \n{}\n"
    "Number of features selected: {}\n"
    "Selected features indices (variables dict):\n{}\n"
    "Selected features ids:\n{}\n"
    "Grid results:\n{}\n"
    "Run time: {}"
).format(
    features_names,
    selected_features_n,
    selected_features_indices,
    selected_features_ids,
    selection_results,
    run_time_feature_selection
)

print(msg)
if save_output:
    save_outputs.save_output_file(msg, 'backward_elimination_auto_output_os', '../doc')
    save_outputs.save_estimator_pckl(grid_selector, 'pipeline_grid_selection_os', 'svm')

# save the feature selected df
# train data
selected_df = df.copy()
X_selected = selected_df.drop(columns=['i', 'y'])
y_selected = selected_df['y']
X_selected = pd.DataFrame(feature_selector.transform(X_selected).toarray(), columns=selected_features_ids)
ids_selected = selected_df.iloc[:, 0]
X_selected.insert(0, 'i', ids_selected)
save_outputs.save_dataframes(X_selected, y_selected, 'train_preprocessed_feature_drop_auto_os')

# test data
df_test = pd.read_csv(base_dir + '/data/test.csv')
df_test.drop(columns=columns_to_drop, inplace=True)
X_selected_test = df_test.drop(columns=['i', 'y'])
y_selected_test = df_test['y']
X_selected_test = pd.DataFrame(feature_selector.transform(X_selected_test).toarray(), columns=selected_features_ids)
ids_selected_test = df_test.iloc[:, 0]
X_selected_test.insert(0, 'i', ids_selected_test)
save_outputs.save_dataframes(X_selected_test, y_selected_test, 'test_preprocessed_feature_drop_auto_os')

# delete the temporary cache before exiting
rmtree(cachedir)

###### modelling
start_time_modelling = datetime.now()

## crete train and validation data sets (uses the df computed above)
df = pd.read_csv(base_dir + '/data/train_preprocessed_feature_drop_auto_os.csv')
X = df.drop(columns=['i', 'y'])
y = df['y']

validation_size = 0.3
X_train, X_validate, y_train, y_validate = train_test_split(X, y, test_size=validation_size, stratify=y)

# no transformation to perform (using transformed data set already transformed, computed in the pipe above):
# define some classifiers to use in the ensemble

def search_classifier(clf, grid_params):
    classifier = GridSearchCV(
        estimator=clf,
        param_grid=grid_params,
        scoring='accuracy',
        n_jobs=-1,
        return_train_score=True
    )
    classifier.fit(X_train, y_train)
    model_pred = classifier.predict(X_validate)
    metrics = evaluation.get_metrics(y_validate, model_pred)
    best_results = pd.DataFrame(classifier.cv_results_)
    best_results = best_results.sort_values("mean_test_score", ascending=False)
    best_estimator = classifier.best_estimator_
    best_parameters = classifier.best_params_

    return model_pred, metrics, best_results, best_estimator, best_parameters


# svm
clf_svm = modelling.Classifiers_sk.svm.value
svm_grid_params = {
    'kernel': ('linear', 'rbf'),
    'C': (0.01, 0.1, 1, 10)
}
svc_model_pred, svc_metrics, svc_best_results, svc_best_estimator, svc_best_params = search_classifier(clf_svm, svm_grid_params)

print('SVC: {}'.format(svc_metrics))

time_svm_end = datetime.now()
run_time_svm = time_svm_end - start_time_modelling

# ann
clf_ann = modelling.Classifiers_sk.mlp.value
ann_grid_params = {
    'hidden_layer_sizes': [(20, 50, 15), (50, 10, 50), (10, 30, 10), (50, 50, 50)],
    'activation': ['tanh', 'relu'],
    'alpha': [0.0001, 0.01, 0.05],
    'solver': ['lbfgs'],
    'max_iter': [500, 1000, 10000]
}
ann_model_pred, ann_metrics, ann_best_results, ann_best_estimator, ann_best_params = search_classifier(clf_ann, ann_grid_params)

print('ANN: {}'.format(ann_metrics))

time_ann_end = datetime.now()
run_time_ann = time_ann_end - time_svm_end

# lr
clf_lr = modelling.Classifiers_sk.lr.value
lr_grid_params = {}
lr_model_pred, lr_metrics, lr_best_results, lr_best_estimator, lr_best_params = search_classifier(clf_lr, lr_grid_params)

print('LR: {}'.format(lr_metrics))

time_lr_end = datetime.now()
run_time_lr = time_lr_end - time_ann_end

# rf
clf_rf = modelling.Classifiers_sk.e_rf.value
rf_grid_params = {
    'criterion': ['entropy', 'gini'],
    'n_estimators': [50, 100, 200],
    'max_depth': [10, 20, 50, 100, None],
    'min_samples_split': [2, 4, 10, 20],
    'max_features': ['auto', 'sqrt', 'log2']
}
rf_model_pred, rf_metrics, rf_best_results, rf_best_estimator, rf_best_params = search_classifier(clf_rf, rf_grid_params)

print('RF: {}'.format(rf_metrics))

time_rf_end = datetime.now()
run_time_rf = time_rf_end - time_lr_end

# gbt
clf_gbt = modelling.Classifiers_sk.e_gtb.value
gbt_grid_params = {
    'learning_rate': [0.001, 0.01, 0.1, 0.5],
    'n_estimators': [50, 100, 200],
    'max_depth': [3, 9, 20, 50, 100, None],
    'min_samples_split': [2, 4, 10, 20],
    'max_features': ['auto', 'sqrt', 'log2']
}
gbt_model_pred, gbt_metrics, gbt_best_results, gbt_best_estimator, gbt_best_params = search_classifier(clf_gbt, gbt_grid_params)

print('GBT: {}'.format(gbt_metrics))

time_gbt_end = datetime.now()
run_time_gbt = time_gbt_end - time_rf_end


# ensemble of the classifiers
def search_ensemble(clf, grid_params):
    classifier = GridSearchCV(
        estimator=clf,
        param_grid=grid_params,
        scoring='accuracy',
        n_jobs=-1,
        return_train_score=True
    )
    classifier.fit(X_train, y_train)
    model_pred = classifier.predict(X_validate)
    metrics = evaluation.get_metrics(y_validate, model_pred)
    best_results = pd.DataFrame(classifier.cv_results_)
    best_results = best_results.sort_values("mean_test_score", ascending=False)
    best_estimator = classifier.best_estimator_
    best_parameters = classifier.best_params_

    return model_pred, metrics, best_results, best_estimator, best_parameters


eclf = VotingClassifier(
    estimators=[
        ('svm', clf_svm),
        ('ann', clf_ann),
        ('lr', clf_lr),
        ('rf', clf_rf),
        ('gbt', clf_gbt)
    ],
    voting='hard')

ensemble_grid_params = {
    'svm__kernel': ('linear', 'rbf'),
    'svm__C': (0.01, 0.1, 1, 10),
    'ann__hidden_layer_sizes': [(20, 50, 15), (50, 10, 50), (10, 30, 10), (50, 50, 50)],
    'ann__activation': ['tanh', 'relu'],
    'ann__alpha': [0.0001, 0.01, 0.05],
    'ann__solver': ['lbfgs'],
    'ann__max_iter': [500, 1000, 10000],
    'rf__criterion': ['entropy', 'gini'],
    'rf__n_estimators': [50, 100, 200],
    'rf__max_depth': [10, 20, 50, 100, None],
    'rf__min_samples_split': [2, 4, 10, 20],
    'rf__max_features': ['auto', 'sqrt', 'log2'],
    'gbt__learning_rate': [0.001, 0.01, 0.1, 0.5],
    'gbt__n_estimators': [50, 100, 200],
    'gbt__max_depth': [3, 9, 20, 50, 100, None],
    'gbt__min_samples_split': [2, 4, 10, 20],
    'gbt__max_features': ['auto', 'sqrt', 'log2']
}

eclf_model_pred, eclf_metrics, eclf_best_results, eclf_best_estimator, eclf_best_params = search_ensemble(eclf, ensemble_grid_params)

print('Ensemble: {}'.format(eclf_metrics))

time_ensemble_end = datetime.now()
run_time_ensemble = time_ensemble_end - time_gbt_end

# set best classifier (considering accuracy):
best_classifiers = [svc_best_estimator, ann_best_estimator, lr_best_estimator, rf_best_estimator, gbt_best_estimator]
best_accuracies = [svc_metrics[0], ann_metrics[0], lr_metrics[0], rf_metrics[0], gbt_metrics[0]]
max_score_index = best_accuracies.index(max(best_accuracies))
best_score = best_accuracies[max_score_index]
best_classifier = best_classifiers[max_score_index]

msg = (
    "****************** GRID SEARCH ******************\n"
    "****************** SVC" 
    "Metrics:\n{}\nBest params:\n{}\nRun time:{}"
    "****************** ANN"
    "Metrics ANN:\n{}\nBest params:\n{}\nRun time:{}"
    "****************** LR"
    "Metrics LR:\n{}\nBest params:\n{}\nRun time:{}"
    "****************** RF"
    "Metrics RF:\n{}\nBest params:\n{}\nRun time:{}"
    "****************** GBT"
    "Metrics GBT:\n{}\nBest params:\n{}\nRun time:{}"
    "****************** Ensemble"
    "Metrics Ensemble:\n{}\nBest params:\n{}\nRun time:{}"
    "****************** Selected best individual classifier"
    "Index: {}\nScore: {}\nClass: {}\n"
).format(
    svc_metrics, svc_best_params, run_time_svm,
    ann_metrics, ann_best_params, run_time_ann,
    lr_metrics, lr_best_params, run_time_lr,
    rf_metrics, rf_best_params, run_time_rf,
    gbt_metrics, gbt_best_params, run_time_gbt,
    eclf_metrics, eclf_best_params, run_time_ensemble,
    max_score_index, best_score, best_classifier
)

print(msg)
if save_output:
    save_outputs.save_output_file(msg, 'results_grid_class_and_ensemble_preprocessed_data_feature_drop_os', '../doc')

# train individual best performer and ensemble in the whole training data set:
best_classifier.fit(X, y)
eclf_best_estimator.fit(X, y)

df_test = pd.read_csv(base_dir + '/data/test_preprocessed_feature_drop_auto_os.csv')
X_test = df_test.drop(columns=['i', 'y'])
y_test = df_test['y']

preds_best_classifier = best_classifier.predict(X_test)

# predict with ensemble classifier
preds_ensemble = eclf_best_estimator.predict(X_test)

# submission
if save_output:
    save_outputs.prepare_submission(df_test.iloc[:, 0], preds_best_classifier, filename='submission_grid_best_classifier_auto_os')
    save_outputs.prepare_submission(df_test.iloc[:, 0], preds_ensemble, filename='submission_grid_ensemble_classifier_auto_os')

# save classifiers
if save_output:
    save_outputs.save_estimator_pckl(best_classifier, 'grid_auto_os', 'selected_best')
    save_outputs.save_estimator_pckl(eclf_best_estimator, 'grid_auto_os', 'ensemble')
