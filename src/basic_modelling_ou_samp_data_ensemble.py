import pandas as pd
from sklearn.model_selection import train_test_split

from modelling import modelling, evaluation
from utils import save_outputs

base_dir = ".."
train_us = pd.read_csv(base_dir + '/data/train_us.csv')
train_os = pd.read_csv(base_dir + '/data/train_os.csv')
seed = 101101011
save_output = True


############
# Oversampled minority
############
## crete train and test data sets
X_os = train_os.drop(columns=['y'])
Y_os = train_os['y']

test_size = 0.3

X_train_os, X_test_os, y_train_os, y_test_os = train_test_split(X_os, Y_os, test_size=test_size, random_state=seed, stratify=Y_os)

num_col_names = [name for name in train_os.columns.tolist() if name.startswith('x')]
cat_col_names = [name for name in train_os.columns.tolist() if name.startswith('c')]
ord_col_names = [name for name in train_os.columns.tolist() if name.startswith('o')]

engineered_data = modelling.FeatureEngineering(num_col_names, cat_col_names, ord_col_names, True,
                                             X_train_os, y_train_os)

# Gradient Boosting Classifier
egtb_model_os = modelling.Model('e_gtb',
                        engineered_data.preprocessor)
egtb_model_os.fit_model(X_train_os, y_train_os)
egtb_model_pred_os = egtb_model_os.full_pipeline.predict(X_test_os)
egtb_pipe_metrics_os = evaluation.get_metrics(y_test_os, egtb_model_pred_os)
egtb_metrics_msg_os = (
    "Metrics for Gradient Boosting:\n"
    "Accuracy: {}\n"
    "Precision: {}\n"
    "Recall: {}\n"
    "F1 score: {}\n"
).format(
    round(egtb_pipe_metrics_os[0], 3),
    round(egtb_pipe_metrics_os[1], 3),
    round(egtb_pipe_metrics_os[2], 3),
    round(egtb_pipe_metrics_os[3], 3)
)

print(egtb_metrics_msg_os)

"""
############
# Undersampled minority
############
## crete train and test data sets
X_us = train_us.drop(columns=['y'])
Y_us = train_us['y']

test_size = 0.3

X_train_us, X_test_us, y_train_us, y_test_us = train_test_split(X_us, Y_us, test_size=test_size, random_state=seed, stratify=Y_us)

num_col_names = [name for name in train_us.columns.tolist() if name.startswith('x')]
cat_col_names = [name for name in train_us.columns.tolist() if name.startswith('c')]
ord_col_names = [name for name in train_us.columns.tolist() if name.startswith('o')]

engineered_data = modelling.FeatureEngineering(num_col_names, cat_col_names, ord_col_names, True,
                                             X_train_us, y_train_us)

# Gradient Boosting Classifier
egtb_model_us = modelling.Model('e_gtb',
                        engineered_data.preprocessor)
egtb_model_us.fit_model(X_train_us, y_train_us)
egtb_model_pred_us = egtb_model_us.full_pipeline.predict(X_test_us)
egtb_pipe_metrics_us = evaluation.get_metrics(y_test_us, egtb_model_pred_us)
egtb_metrics_msg_us = (
    "Metrics for Gradient Boosting:\n"
    "Accuracy: {}\n"
    "Precision: {}\n"
    "Recall: {}\n"
    "F1 score: {}\n"
).format(
    round(egtb_pipe_metrics_us[0], 3),
    round(egtb_pipe_metrics_us[1], 3),
    round(egtb_pipe_metrics_us[2], 3),
    round(egtb_pipe_metrics_us[3], 3)
)

print(egtb_metrics_msg_us)
"""
############
# Test
############
test = pd.read_csv(base_dir + '/data/test.csv')
X_test = test.drop(columns=['i', 'y'])
preds = egtb_model_os.full_pipeline.predict(X_test)

# submission
if save_output:
    save_outputs.prepare_submission(test.iloc[:,0], preds, filename='submission_basic_ensem_os')