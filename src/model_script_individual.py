import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import VotingClassifier

from modelling import modelling, evaluation
from utils import save_outputs

base_dir = ".."
df = pd.read_csv(base_dir + '/data/train_preprocessed_feature_drop.csv')
seed = 101101011
save_output = True

## crete train and test data sets
X = df.drop(columns=['y'])
Y = df['y']

test_size = 0.3

X_train, X_validate, y_train, y_validate = train_test_split(X, Y, test_size=test_size, random_state=seed, stratify=Y)

# no transformation to perform (loads transformed data set already):
# define some classifiers to use in the ensemble

def get_classifier(clf):
    clf.fit(X_train, y_train)
    model_pred = clf.predict(X_validate)
    metrics = evaluation.get_metrics(y_validate, model_pred)

    return model_pred, metrics

# svm
clf_svm = modelling.Classifiers_sk.svm.value
svc_model_pred, svc_metrics = get_classifier(clf_svm)

# ann
clf_ann = modelling.Classifiers_sk.mlp.value
ann_model_pred, ann_metrics = get_classifier(clf_ann)

# lr
clf_lr = modelling.Classifiers_sk.lr.value
lr_model_pred, lr_metrics = get_classifier(clf_lr)

# rf
clf_rf = modelling.Classifiers_sk.e_rf.value
rf_model_pred, rf_metrics = get_classifier(clf_rf)

# gbt
clf_gbt = modelling.Classifiers_sk.e_rf.value
gbt_model_pred, gbt_metrics = get_classifier(clf_gbt)

print(round(svc_metrics[0], 3), round(svc_metrics[1], 3), round(svc_metrics[2], 3), round(svc_metrics[3], 3))
print(round(ann_metrics[0], 3), round(ann_metrics[1], 3), round(ann_metrics[2], 3), round(ann_metrics[3], 3))
print(round(lr_metrics[0], 3), round(lr_metrics[1], 3), round(lr_metrics[2], 3), round(lr_metrics[3], 3))
print(round(rf_metrics[0], 3), round(rf_metrics[1], 3), round(rf_metrics[2], 3), round(rf_metrics[3], 3))
print(round(gbt_metrics[0], 3), round(gbt_metrics[1], 3), round(gbt_metrics[2], 3), round(gbt_metrics[3], 3))

# ensemble of the classifiers
eclf = VotingClassifier(
    estimators=[
        ('svm', clf_svm),
        ('ann', clf_ann),
        ('lr', clf_lr),
        ('rf', clf_rf),
        ('gbt', clf_gbt)
    ],
    voting='hard')

eclf_model_pred, eclf_metrics = get_classifier(eclf)

print(round(eclf_metrics[0], 3), round(eclf_metrics[1], 3), round(eclf_metrics[2], 3), round(eclf_metrics[3], 3))

test = pd.read_csv(base_dir + '/data/test_preprocessed_feature_drop.csv')
X_test = test.drop(columns=['i', 'y'])
preds = eclf.predict(X_test)


# submission
if save_output:
    save_outputs.prepare_submission(test.iloc[:, 0], preds, filename='submission_ensemble_classifier')