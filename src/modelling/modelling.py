# general
from enum import Enum
import numpy as np
import pandas as pd

# pre processing
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.impute import SimpleImputer

# classifiers
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier


class Classifiers_sk(Enum):
    """
    Enum for implemented classifiers using sklearn.

    Will implement:
    svm - Support Vector Machine classifier
    dt - Decision Tree Classifier
    lr - Logistic Regression Classifier
    knn - K-Nearest Neighbors Classifier
    nb - Naive Bayes Classifier
    mlp - Multi-layer Perceptron classifier
    e_rf - Random Forest Classifier
    e_ada - Ada Boost Classifier
    e_gtb - Gradient Tree Boosting

    see:
    https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html
    """

    svm = SVC()
    dt = DecisionTreeClassifier()
    lr = LogisticRegression()
    knn = KNeighborsClassifier()
    nb = GaussianNB()
    mlp = MLPClassifier()
    e_rf = RandomForestClassifier()
    e_ada = AdaBoostClassifier()
    e_gtb = GradientBoostingClassifier()


class Model:
    def __init__(self,
                 classifier_str,
                 pre_processing,
                 model_params=None, grid_params=None):
        self.clf_str = classifier_str
        self.pre_processing = pre_processing
        self.model_params = model_params
        self.grid_params = grid_params
        for name, member in Classifiers_sk.__members__.items():
            if name == self.clf_str:
                self.clf = member.value

        self.full_pipeline = Pipeline(
            steps=[
                ('preprocessor', self.pre_processing),
                ('classifier', self.clf)
            ]
        )

    def fit_model(self, features, target):
        self.full_pipeline.fit(features, target)


    def grid_search(self, train_features, train_target, grid_params=None):
        from sklearn.model_selection import GridSearchCV

        self.grid_params = grid_params

        # settings
        grid = GridSearchCV(self.clf, self.grid_params, cv=5, verbose=0, n_jobs=-1)

        # model train
        self.fit_clf = grid.fit(train_features, train_target)

        self.best_params = grid.best_params_
        self.best_estimator = grid.best_estimator_

        return self.best_params, self.best_estimator


class FeatureEngineering:
    def __init__(self,
                 numeric_features, categorical_features, ordinal_features, ordinal_as_cat,
                 train_features, train_target,
                 numeric_transformer=None, categorical_transformer=None):
        """

        """
        if ordinal_as_cat:
            self.categorical_features = categorical_features + ordinal_features
            self.numeric_features = numeric_features
        else:
            self.categorical_features = categorical_features
            self.numeric_features = numeric_features + ordinal_features

        self.train_features = train_features
        self.train_target = train_target

        # numeric variables transformer
        if numeric_transformer is None:
            self.numeric_transformer = Pipeline(
                steps=[
                    ("imputer", SimpleImputer(strategy="median")),
                    ("scaler", StandardScaler())]
            )
        else:
            self.numeric_transformer = numeric_transformer

        # categorical variables transformer
        if categorical_transformer is None:
            self.categorical_transformer = Pipeline(
                steps=[
                    ('onehot', OneHotEncoder(handle_unknown="ignore"))
                ]
            )
        else:
            self.categorical_transformer = categorical_transformer

        self.preprocessor = ColumnTransformer(
            transformers=[
                ('num', self.numeric_transformer, self.numeric_features),
                ('cat', self.categorical_transformer, self.categorical_features)
            ]
        )

        # only training data is used to fit the pre-processing transformer (no data leakage)
        #self.preprocessor.fit(self.train_data)

        # after the fitting process the variable names are lost. recover:
        #self.fit_preproc_categories = self.preprocessor.named_transformers_['cat'].named_steps[
        #    'onehot'].get_feature_names()
        #self.fit_preproc_vars = np.append(self.num_vars, self.fit_preproc_categories)

    def fit_test(self, test_data):
        """
        utility function
        """
        self.X_test_enc = pd.DataFrame(self.preprocessor.transform(test_data))
