from datetime import datetime
from tempfile import mkdtemp
from shutil import rmtree
from joblib import Memory

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import SequentialFeatureSelector
from sklearn.ensemble import VotingClassifier

from modelling import modelling, evaluation
from preprocessing import feature_engineering
from utils import save_outputs

base_dir = ".."
save_output = True

## crete train and validation data sets (uses the df computed above)
df = pd.read_csv(base_dir + '/data/train_preprocessed_feature_drop_auto.csv')
X = df.drop(columns=['i', 'y'])
y = df['y']

validation_size = 0.3
X_train, X_validate, y_train, y_validate = train_test_split(X, y, test_size=validation_size, stratify=y)

# no transformation to perform (using transformed data set already transformed, computed in the pipe above):
# define some classifiers to use in the ensemble

start_time_ensemble = datetime.now()

# svm
clf_svm = modelling.Classifiers_sk.svm.value

# lr
clf_lr = modelling.Classifiers_sk.lr.value

# gbt
clf_gbt = modelling.Classifiers_sk.e_gtb.value

# ensemble of the classifiers
def search_ensemble(clf, grid_params):
    classifier = GridSearchCV(
        estimator=clf,
        param_grid=grid_params,
        scoring='accuracy',
        n_jobs=-1,
        return_train_score=True
    )
    classifier.fit(X_train, y_train)
    model_pred = classifier.predict(X_validate)
    metrics = evaluation.get_metrics(y_validate, model_pred)
    best_results = pd.DataFrame(classifier.cv_results_)
    best_results = best_results.sort_values("mean_test_score", ascending=False)
    best_estimator = classifier.best_estimator_
    best_parameters = classifier.best_params_

    return model_pred, metrics, best_results, best_estimator, best_parameters


eclf = VotingClassifier(
    estimators=[
        ('svm', clf_svm),
        ('lr', clf_lr),
        ('gbt', clf_gbt)
    ],
    voting='hard')

ensemble_grid_params = {
    'svm__kernel': ['rbf'],
    'svm__C': [1],
    'gbt__learning_rate': [0.1],
    'gbt__n_estimators': [100],
    'gbt__max_depth': [3],
    'gbt__min_samples_split': [4],
    'gbt__max_features': ['log2']
}

eclf_model_pred, eclf_metrics, eclf_best_results, eclf_best_estimator, eclf_best_params = search_ensemble(eclf, ensemble_grid_params)

print('Ensemble: {}'.format(eclf_metrics))

time_ensemble_end = datetime.now()
run_time_ensemble = time_ensemble_end - start_time_ensemble

msg = (
    "****************** GRID SEARCH Ensemble - TOP 3 ******************\n"
    "****************** Ensemble"
    "Metrics Ensemble:\n{}\nBest params:\n{}\nRun time:{}"
).format(
    eclf_metrics, eclf_best_params, run_time_ensemble,
)

print(msg)
if save_output:
    save_outputs.save_output_file(msg, 'results_grid_class_and_ensemble_preprocessed_data_feature_drop', '../doc')


# train individual best performer and ensemble in the whole training data set:
eclf_best_estimator.fit(X, y)

# save classifiers
if save_output:
    save_outputs.save_estimator_pckl(eclf_best_estimator, 'grid_auto', 'ensemble_top_3')

df_test = pd.read_csv(base_dir + '/data/test_preprocessed_feature_drop_auto.csv')
X_test = df_test.drop(columns=['i', 'y'])
y_test = df_test['y']

# predict with ensemble classifier
preds_ensemble = eclf_best_estimator.predict(X_test)

# submission
if save_output:
    save_outputs.prepare_submission(df_test.iloc[:, 0], preds_ensemble, filename='submission_grid_ensemble_classifier_top3')
