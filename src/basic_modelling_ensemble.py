import pandas as pd
from sklearn.model_selection import train_test_split

from modelling import modelling, evaluation
from utils import save_outputs

base_dir = ".."
train = pd.read_csv(base_dir + '/data/train.csv')
seed = 101101011
save_output = False

## crete train and test data sets
X = train.drop(columns=['i', 'y'])
Y = train['y']

test_size = 0.3

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=test_size, random_state=seed, stratify=Y)

num_col_names = [name for name in train.columns.tolist() if name.startswith('x')]
cat_col_names = [name for name in train.columns.tolist() if name.startswith('c')]
ord_col_names = [name for name in train.columns.tolist() if name.startswith('o')]

engineered_data = modelling.FeatureEngineering(num_col_names, cat_col_names, ord_col_names, True,
                                             X_train, y_train)
# Random Forest
erf_model = modelling.Model('e_rf',
                        engineered_data.preprocessor)
erf_model.fit_model(X_train, y_train)
erf_model_pred = erf_model.full_pipeline.predict(X_test)
erf_pipe_metrics = evaluation.get_metrics(y_test, erf_model_pred)
erf_metrics_msg = (
    "Metrics for Random Forest:\n"
    "Accuracy: {}\n"
    "Precision: {}\n"
    "Recall: {}\n"
    "F1 score: {}\n"
).format(
    round(erf_pipe_metrics[0], 3),
    round(erf_pipe_metrics[1], 3),
    round(erf_pipe_metrics[2], 3),
    round(erf_pipe_metrics[3], 3)
)

# Ada boost
eada_model = modelling.Model('e_ada',
                             engineered_data.preprocessor)
eada_model.fit_model(X_train, y_train)
eada_model_pred = eada_model.full_pipeline.predict(X_test)
eada_pipe_metrics = evaluation.get_metrics(y_test, eada_model_pred)
eada_metrics_msg = (
    "Metrics for Ada Boost:\n"
    "Accuracy: {}\n"
    "Precision: {}\n"
    "Recall: {}\n"
    "F1 score: {}\n"
).format(
    round(eada_pipe_metrics[0], 3),
    round(eada_pipe_metrics[1], 3),
    round(eada_pipe_metrics[2], 3),
    round(eada_pipe_metrics[3], 3)
)

# Gradient Boosting Classifier
egtb_model = modelling.Model('e_gtb',
                        engineered_data.preprocessor)
egtb_model.fit_model(X_train, y_train)
egtb_model_pred = egtb_model.full_pipeline.predict(X_test)
egtb_pipe_metrics = evaluation.get_metrics(y_test, egtb_model_pred)
egtb_metrics_msg = (
    "Metrics for Gradient Boosting:\n"
    "Accuracy: {}\n"
    "Precision: {}\n"
    "Recall: {}\n"
    "F1 score: {}\n"
).format(
    round(egtb_pipe_metrics[0], 3),
    round(egtb_pipe_metrics[1], 3),
    round(egtb_pipe_metrics[2], 3),
    round(egtb_pipe_metrics[3], 3)
)

print(erf_metrics_msg, eada_metrics_msg, egtb_metrics_msg)

test = pd.read_csv(base_dir + '/data/test.csv')
X_test = test.drop(columns=['i', 'y'])
preds = egtb_model.full_pipeline.predict(X_test)

# submission
if save_output:
    save_outputs.prepare_submission(test.iloc[:,0], preds, filename='submission_baseline_ensemble')