import pandas as pd
from sklearn.model_selection import train_test_split

from modelling import modelling, evaluation
from utils import save_outputs

base_dir = ".."
train = pd.read_csv(base_dir + '/data/train.csv')
seed = 101101011
save_output = False

## crete train and test data sets
X = train.drop(columns=['i', 'y'])
Y = train['y']

test_size = 0.3

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=test_size, random_state=seed, stratify=Y)

num_col_names = [name for name in train.columns.tolist() if name.startswith('x')]
cat_col_names = [name for name in train.columns.tolist() if name.startswith('c')]
ord_col_names = [name for name in train.columns.tolist() if name.startswith('o')]

engineered_data = modelling.FeatureEngineering(num_col_names, cat_col_names, ord_col_names, True,
                                             X_train, y_train)

dt_model = modelling.Model('dt',
                        engineered_data.preprocessor)

dt_model.fit_model(X_train, y_train)

dt_model_pred = dt_model.full_pipeline.predict(X_test)

dt_pipe_metrics = evaluation.get_metrics(y_test, dt_model_pred)

print(round(dt_pipe_metrics[0],3), round(dt_pipe_metrics[1],3), round(dt_pipe_metrics[2],3), round(dt_pipe_metrics[3],3))

test = pd.read_csv(base_dir + '/data/test.csv')
X_test = test.drop(columns=['i', 'y'])
preds = dt_model.full_pipeline.predict(X_test)

# submission
if save_output:
    save_outputs.prepare_submission(test.iloc[:,0], preds, filename='submission_baseline')