import pickle
import pandas as pd
import matplotlib.pyplot as plt

base_dir = ".."
base_output_dir = base_dir + "/doc"

def save_output_file(msg, filename, save_dir=None, fileextension='.txt', override=False):
    write_mode = 'w' if override else 'a'
    if save_dir is None:
        output_file = base_output_dir + '/' + filename + fileextension
    else:
        output_file = save_dir + '/' + filename + fileextension

    with open(output_file, write_mode) as f:
        print(msg, file=f)


def save_output_plots(pltname, save_dir=None, pltextension=".svg"):
    if save_dir is None:
        plot_dir = base_output_dir + '/plots/' + pltname + pltextension
    else:
        plot_dir = save_dir + '/' + pltname + pltextension

    plt.savefig(plot_dir, transparent=True, bbox_inches='tight')

def save_dataframes(X, y, filename, dir=base_dir):
    new_df = X.assign(y=y)
    new_df.to_csv(path_or_buf=dir + '/data/' + filename + '.csv', index=False)

def save_estimator_pckl(clf, model_type, model_id, relative_models_dir='../models/'):
    with open("{}{}_{}.pkl".format(relative_models_dir, model_type, model_id), 'wb') as f:
        pickle.dump(clf, f)

def load_estimator_pckl(model_type, model_id, relative_models_dir='../models/'):
    with open("{}{}_{}.pkl".format(relative_models_dir, model_type, model_id), 'rb') as f:
        return pickle.load(f)



def prepare_submission(ids, predictions, filename='submission'):
    subm = pd.DataFrame(data={'i': ids, 'y': predictions})
    subm.to_csv(path_or_buf=base_dir + '/submission/' + filename + '.csv', index=False)
    return subm
