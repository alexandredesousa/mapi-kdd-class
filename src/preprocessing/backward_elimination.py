from datetime import datetime

import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.feature_selection import SequentialFeatureSelector

from tempfile import mkdtemp
from shutil import rmtree
from joblib import Memory

from src.modelling import modelling, evaluation
from src.utils import save_outputs


# see:
# https://scikit-learn.org/stable/modules/feature_selection.html#sequential-feature-selection
# https://scikit-learn.org/stable/modules/feature_selection.html#feature-selection-as-part-of-a-pipeline
# https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html
# https://www.tomasbeuzen.com/post/scikit-learn-gridsearch-pipelines/
# https://stackoverflow.com/a/55629709
# https://github.com/scikit-learn/scikit-learn/issues/7536#issuecomment-251236645
# https://stackoverflow.com/a/47389468
# https://scikit-learn.org/stable/auto_examples/compose/plot_compare_reduction.html#caching-transformers-within-a-pipeline
# https://github.com/scikit-learn/scikit-learn/issues/7536#issuecomment-251236645

start_time = datetime.now()

base_dir = "../.."
train = pd.read_csv(base_dir + '/data/train.csv')
seed = 101101011
save_output = True

# Create a temporary folder to store the transformers of the pipeline
cachedir = mkdtemp()
memory = Memory(location=cachedir, verbose=3)

X = train.drop(columns=['i', 'y'])
Y = train['y']

num_col_names = [name for name in train.columns.tolist() if name.startswith('x')]
cat_col_names = [name for name in train.columns.tolist() if name.startswith('c')]
ord_col_names = [name for name in train.columns.tolist() if name.startswith('o')]

n_features = len(num_col_names) + len(cat_col_names) + len(ord_col_names)

clf = modelling.Classifiers_sk.svm.value
sfs = SequentialFeatureSelector(clf, direction='backward', n_features_to_select=10, n_jobs=-1)

engineered_data = modelling.FeatureEngineering(num_col_names, cat_col_names, ord_col_names, True,
                                                   X, Y)

pipe = Pipeline([
    ('preprocessing', engineered_data.preprocessor),
    ('feature_selection', sfs),
    ('classifier', clf)
], memory=memory)

search_space = [
    {'feature_selection__n_features_to_select': [736]}
]

clf_select = GridSearchCV(pipe, search_space, cv=5, n_jobs=-1, verbose=4, scoring='accuracy')
clf_select.fit(X, Y)

# Results
best_estimator = clf_select.best_estimator_
best_params = clf_select.best_params_
results = clf_select.cv_results_
pd_results = pd.DataFrame(clf_select.cv_results_)
pd_results = pd_results.sort_values("mean_test_score", ascending=False)

selected_features_indices = best_estimator.named_steps['feature_selection'].support_
#selected_features_names = best_estimator.named_steps['feature_selection'].get_feature_names_out()
best_score = clf_select.best_score_

run_time = datetime.now() - start_time

msg = (
    "****************** BACKWARD ELIMINATION ******************\n"
    "Best Score (accuracy):\n{}\n"
    "Variables dict (indices):\n{}\n"
    #"Variables dict (names):\n{}\n"
    "Run time: {}"
).format(
    best_score,
    selected_features_indices,
    #selected_features_names,
    run_time
)

print(msg)
save_outputs.save_output_file(msg, 'backward_elimination_output')

############
# Test
############
test = pd.read_csv(base_dir + '/data/test.csv')
X_test = test.drop(columns=['i', 'y'])
preds = best_estimator.predict(X_test)

# submission
if save_output:
    save_outputs.prepare_submission(test.iloc[:, 0], preds, filename='submission_basic_feature_red')

# delete the temporary cache before exiting
rmtree(cachedir)