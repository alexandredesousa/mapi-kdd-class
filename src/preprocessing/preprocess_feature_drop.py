import pandas as pd

from src.utils import save_outputs
from src.modelling import modelling

base_dir = "../.."
train = pd.read_csv(base_dir + '/data/train.csv')
test = pd.read_csv(base_dir + '/data/test.csv')

# base data
## crete train and test data sets
cat_columns_to_drop = ['c1', 'c2', 'c9', 'c10', 'c11', 'c12']
num_cols_to_drop = ['x2', 'x4', 'x5', 'x6', 'x8', 'x10', 'x12', 'x14', 'x16', 'x20', 'x22', 'x23', 'x24', 'x32', 'x35', 'o2']
columns_to_drop = cat_columns_to_drop + num_cols_to_drop
train.drop(columns=columns_to_drop, inplace=True)
test.drop(columns=columns_to_drop, inplace=True)
X_train = train.drop(columns=['i', 'y'])
y_train = train['y']
X_test = test.drop(columns=['i', 'y'])
y_test = test['y']

num_col_names = [name for name in train.columns.tolist() if name.startswith('x')]
cat_col_names = [name for name in train.columns.tolist() if name.startswith('c')]
ord_col_names = [name for name in train.columns.tolist() if name.startswith('o')]

engineered_data_train = modelling.FeatureEngineering(num_col_names, cat_col_names, ord_col_names, True,
                                             X_train, y_train)

engineered_data_test = modelling.FeatureEngineering(num_col_names, cat_col_names, ord_col_names, True,
                                             X_test, y_test)

# save sparse output as pandas dataframe: https://stackoverflow.com/a/36968458
X_train_processed = pd.DataFrame(engineered_data_train.preprocessor.fit_transform(X_train).toarray())
X_test_processed = pd.DataFrame(engineered_data_train.preprocessor.transform(X_test).toarray())

trasformed_features_target = [
    False,  True,  True,  True,  True,  True,  True,  True,  True,  True, False,  True,
    True,  True,  True,  True,  True,  True, False,  True,  True,  True, False,  True,
    True, False,  True, False,  True,  True,  True,  True,  True, False,  True,  True,
    True,  True, False,  True,  True,  True, False, False,  True,  True,  True,  True,
    True,  True,  True, False,  True,  True,  True,  True,  True,  True,  True,  True,
    False, False,  True,  True,  True,  True,  True,  True,  True,  True,  True,  True,
    True,  True,  True,  True,  True,  True,  True, False,  True,  True,  True,  True,
    True,  True,  True,  True,  True,  True,  True,  True,  True,  True,  True,  True,
    True,  True,  True,  True, False,  True,  True,  True,  True,  True,  True,  True,
    False, False,  True,  True,  True,  True,  True,  True,  True
]

# drop columns - see https://stackoverflow.com/a/57090806
X_train_drop = X_train_processed.loc[:, trasformed_features_target]
X_test_drop = X_test_processed.loc[:, trasformed_features_target]
ids = test.iloc[:, 0]
X_test_drop.insert(0, 'i', ids)


# save train data preprocessed and drop features
save_outputs.save_dataframes(X_train_drop, y_train, 'train_preprocessed_feature_drop', '../../')
# save test data preprocessed and drop features
save_outputs.save_dataframes(X_test_drop, y_test, 'test_preprocessed_feature_drop', '../../')