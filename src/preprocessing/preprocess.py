import pandas as pd
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from collections import Counter # defining the dataset

from src.utils import save_outputs

#base_dir = "../.."
#train = pd.read_csv(base_dir + '/data/train.csv')

# base data
#X = train.drop(columns=['i', 'y'])
#y = train['y']
#print(Counter(y))

def oversampler(X, y):
    # instantiating the random over sampler
    ros = RandomOverSampler()
    # resampling X, y
    X_ros, y_ros = ros.fit_resample(X, y)  # new class distribution
    return X_ros, y_ros

#X_ros, y_ros = oversampler(X, y)
#print(Counter(y_ros))

def undersampler(X, y):
    # instantiating the random undersampler
    rus = RandomUnderSampler(replacement=True)
    # resampling X, y
    X_rus, y_rus = rus.fit_resample(X, y)  # new class distribution
    return X_rus, y_rus

#X_rus, y_rus = undersampler(X, y)
#print(Counter(y_rus))

# save over sampled data
#save_outputs.save_dataframes(X_ros, y_ros, 'train_os_2')
# save under sampled data
#save_outputs.save_dataframes(X_rus, y_rus, 'train_us_2')