# general
from enum import Enum
import numpy as np
import pandas as pd

# pre processing
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler, MinMaxScaler, MaxAbsScaler, RobustScaler,\
    OneHotEncoder, LabelBinarizer
from sklearn.impute import SimpleImputer, MissingIndicator, KNNImputer
# feature selection
from sklearn.feature_selection import SequentialFeatureSelector

from src.modelling import modelling

class PreProcessors(Enum):
    # numerical
    standard = StandardScaler()
    minmax = MinMaxScaler()
    maxabsscaler = MaxAbsScaler()
    robust = RobustScaler()

    # categorical
    onehot = OneHotEncoder(handle_unknown="ignore")
    lblbinarizer = LabelBinarizer()

class Imputers(Enum):
    simple = SimpleImputer(strategy="median")
    #iterative = IterativeImputer()
    missing = MissingIndicator()
    neighbors = KNNImputer()

class FeatureEngineering:
    def __init__(self,
                 numeric_features, categorical_features, ordinal_features, ordinal_as_cat,
                 numeric_transformer=None, categorical_transformer=None):
        """

        """
        if ordinal_as_cat:
            self.categorical_features = categorical_features + ordinal_features
            self.numeric_features = numeric_features
        else:
            self.categorical_features = categorical_features
            self.numeric_features = numeric_features + ordinal_features

        # numeric variables transformer
        if numeric_transformer is None:
            self.numeric_transformer = Pipeline(
                steps=[
                    ("imputer", Imputers.simple.value),
                    ("scaler", PreProcessors.robust.value)]
            )
        else:
            self.numeric_transformer = numeric_transformer

        # categorical variables transformer
        if categorical_transformer is None:
            self.categorical_transformer = Pipeline(
                steps=[
                    ('onehot', PreProcessors.onehot.value)
                ]
            )
        else:
            self.categorical_transformer = categorical_transformer

        self.preprocessor = ColumnTransformer(
            transformers=[
                ('num', self.numeric_transformer, self.numeric_features),
                ('cat', self.categorical_transformer, self.categorical_features)
            ]
        )

        # only training data is used to fit the pre-processing transformer (no data leakage)
        #self.preprocessor.fit(self.train_data)

        # after the fitting process the variable names are lost. recover:
        #self.fit_preproc_categories = self.preprocessor.named_transformers_['cat'].named_steps[
        #    'onehot'].get_feature_names()
        #self.fit_preproc_vars = np.append(self.num_vars, self.fit_preproc_categories)

    def fit_test(self, X, y=None):
        """
        utility function
        """
        self.X_encoded = pd.DataFrame(self.preprocessor.fit_transform(X,y))


class FeaturesSelection:
    def __init__(self, estimator=None, selector=None):
        if estimator is None:
            self.estimator = modelling.Classifiers_sk.svm.value
        else:
            self.estimator = estimator

        if selector is None:
            self.selector = SequentialFeatureSelector()
