# 1 - baseline
* Metrics obtained in validation: 0.679 0.908 0.111 0.197
* Score: 0.68577
* SVC. Basic processing.
* (script: basic_modelling.py)
* (model: basic_model_0_svm.pkl)

# 2 - ensemble baseline
* Metrics obtained in validation: 0.682 0.71 0.184  0.293
* Score: 0.68381
* GTB. Basic processing.
* (script: basic_modelling_ensemble.py)
* (model: basic_model_0_egbt.pkl)

# 3 - basic modelling. over sampling training
* Metrics obtained in validation: 0.586 0.637 0.401 0.492
* Score: 0.63531
* SVC. Oversampling
* (script: basic_modelling_ou_samp_data.py)
* (model: basic_model_oversampled_svm.pkl)

# 4 - basic ensemble modelling. over sampling training
* Metrics obtained in validation: 0.663 0.675 0.628 0.65
* Score: 0.63654
* GTB. Oversampling
* (script: basic_modelling_ou_data_ensemble.py)
* (model: basic_model_oversampled_egtb.pkl)

# 5 - basic prediction. remove some categorical variables
* Metrics obtained in validation: 0.678 0.896 0.111 0.197
* Score: 0.68528
* SVC. Eliminate categoricals
* (script: basic_modelling_eliminate_cat.py)
* (model: basic_model_eliminate_cat_svm.pkl)

# 6 - basic prediction. remove some variables (numerical + categorical)
* Metrics obtained in validation: 0.679 0.909 0.112 0.2
* Score: 0.68552
* SVC. Eliminate categoricals
* (script: basic_modelling_eliminate_columns.py)
* (model: basic_model_eliminate_columns_svm.pkl)

# 7 - prediction with removed variables and feature drop (from feature selection)
* Metrics obtained in validation: 0.679 0.897 0.112 0.199
* Score: 0.68430
* SVC. Feature drop
* (script: model_script_individual.py) (after: modelling_backward_elimination_feature_drop.py to generate dataset with features to drop)
* (model: feature_drop_ind_svm.pkl)

# 8 - prediction with removed variables and feature drop (from feature selection)
* Metrics obtained in validation: 0.683 0.778 0.157 0.261
* Score: 0.68919
* Voting Ensemble (svm, ann, lr, rf, gbt). Feature drop
* (script: model_script_individual.py) (after: modelling_backward_elimination_feature_drop.py to generate dataset with features to drop)
* (model: feature_drop_vote_ens.pkl)

# 9 - Best among others, single estimator, auto selected via grid search (SVM)
* Metrics obtained in validation: 0.681, 0.874, 0.122, 0.214
* Score: 0.68528
* Best selected estimator among 5 - auto selected via grid search - SVM
* (script: model_full.py)
* (model: grid_auto_selected_best.pkl)

# 10 - Selected Ensemble via grid
* Metrics obtained in validation: 0.683, 0.830, 0.141, 0.241
* Score: 0.68674
* Ensemble with 5 models, best estimators selected via grid search (svm, ann, lr, rf, gbt)
* (script: model_full.py)
* (model: grid_auto_ensemble.pkl)

# 11 - Selected ensemble - top 3 classifiers
* Metrics obtained in validation: 0.687, 0.881, 0.143, 0.246
* Score: 0.68479
* Ensemble with top 3 (measured accuracy) of 5 estimators (svm, lr, gbt).
* (script: model_full_ensemble_top_3.py)
* (model: grid_auto_ensemble_top_3.pkl)

# 12 - Best among others, single estimator, auto selected via grid search (ann)
* Metrics obtained in validation: 0.693, 0.743, 0.213, 0.331
* Score: 0.68185
* Best selected estimator among 5 - auto selected via grid search - ANN. oversampled
* (script: model_full_oversampled.py)
* (model: grid_auto_os_selected_best.pkl)

# 13 - Selected Ensemble via grid
* Metrics obtained in validation: 0.679, 0.880, 0.117, 0.207
* Score: 0.68748
* Ensemble with 5 models, best estimators selected via grid search (svm, ann, lr, rf, gbt) - oversampled
* (script: model_full_oversampled.py)
* (model: grid_auto_os_ensemble.pkl)

# 14 - Selected Ensemble via grid - top 3
* Metrics obtained in validation: 0.679, 0.787, 0.136, 0.232
* Score: 0.68870
* Ensemble with top 3 (ann, lr, rf) (measured accuracy) of 5 best estimators selected via grid search (svm, ann, lr, rf, gbt) - oversampled
* (script: model_full_oversampled.py)
* (model: grid_auto_os_top3_ensemble.pkl)

# 15 - Selected Ensemble via grid - weighted
* Metrics obtained in validation: 0.681, 0.828, 0.131, 0.227
* Score: 0.68503
* Ensemble with 5 models, best estimators selected via grid search (svm, ann, lr, rf, gbt) - oversampled - weighted [1, 3, 1, 1, 1] (best, ann)
* (script: model_full_oversampled.py)
* (model: grid_auto_os_weighted_ensemble.pkl)

# 16 - Selected Ensemble via grid - weighted
Metrics obtained in validation: 0.681, 0.810, 0.136, 0.233
Score: 0.68577
Ensemble with 4 models, best estimators selected via grid search (svm, ann, lr, rf, gbt) - oversampled - weighted [1, 2, 1, 1] (best, ann)
(script: model_full_oversampled.py)
(model: grid_auto_os_weighted_2_ensemble.pkl)