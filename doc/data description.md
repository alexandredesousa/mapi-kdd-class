# Description
It is observed that the reporting of information by companies can sometimes suffer some delays.

# Goal
The objective is to see if, based on the available attributes, it is possible to classify companies as being late in the report.
Furthermore, it would be relevant to understand which attributes may be relevant for the attribution of this classification.

# Variable characterization
For this purpose, the database includes continuous variables (variables of the “x” type),
categorical variables (variables of the “c” type), and ordinals (variables of the “o” type).
